#MadLib Program

def madlib():
    print ("Thank you for playing my Mad Lib Game")
    adj1 = input ("Please enter an adjective")
    noun1 = input ("Please enter an animal")
    noun2 = input ("Please enter a noun")
    noun3 = input ("Please enter the name of your best friend")
    adj2 = input ("Please enter an adjective")
    verb1 = input ("Please enter a verb ending in ing")
    verb2 = input ("Please enter a verb ending in ing")
    emotion1 = input ("Please enter an emotion ending in ed")

    print ("I started my day by going to Great Bay Community College where I had a " + adj1 + " time! I met up with all my friends and I got to see a furry " + noun1 + ", on my way in. After that, I went to my first class where a " + noun2 + " was waiting for me. This took me by surprise, but I ignored it and sat down next to " + noun3 + ". We joked about the professors' " + adj2 + " hair, while he discussed our homework. He passed it back while he was " + verb1 + " up the stairs. He was also " + verb2 + " when he handed me my homework, which I got a 60 on. I was so " + emotion1 + ", that I marched right out. I decided that this class wasn't for me, so I dropped it and hung out with the " + noun1 + " all day.") 
    
madlib()
