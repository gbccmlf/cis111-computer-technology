var canvas = document.querySelector('canvas');
var surface = canvas.getContext('2d');
var bee = document.getElementById('bee');
var x = 0;
var y = 0;
var direction = 1;

function moveIt() {
    
    surface.fillStyle = 'white';
    surface.fillRect(0,0,500,500);
    
    surface.drawImage(bee, x, y);
    
    if(x > 500 || x < 0){
        direction = -direction;
    }
    
    x = x + direction;
    y = y + direction;
    requestAnimationFrame(moveIt);
}

moveIt();

var canvas = document.getElementById("myCanvas2");
var ctx = canvas.getContext("2d");
        ctx.fillStyle = "#FE0506";
        ctx.fillRect(200,200,150,75);
        
    